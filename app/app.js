const express = require('express');
const cors = require('cors');
const app = express();

app.use(cors());

app.get('/', (req, res) => {
    res.send('application is working and got updated. Hi Allison');
})

app.get('/contents', (req, res) => {
    res.status(200);
    res.json({
        "pageTitle": "Angular Application",
        "pageBody": "This is calling api and getting content"
    });
})

module.exports = app;